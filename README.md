# jumpcore

Jumpcore is a C++ game framework capable of exporting to Windows, Mac, Linux, iPhone, WebOS, and Android.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/jumpcore).**
